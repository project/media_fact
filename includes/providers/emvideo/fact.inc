<?php
/**
 * @file
 *  Provide support for the FACT provider to the emfield module.
 */

/**
 *  This is the main URL for FACT.tv.
 */
define('EMVIDEO_FACT_MAIN_URL', 'http://fact.tv/');

/**
 *  This defines the version of the content data array that we serialize
 *  in emvideo_fact_data().
 */
define('EMVIDEO_FACT_DATA_VERSION', 1);

/**
 * Implementation of emvideo_fact_info().
 * This returns information relevant to a specific 3rd party video provider.
 *
 * @return
 *   an array of strings requested by various admin and other forms
 *   'name' => the translated name of the provider
 *   'url' => the url to the main page for the provider
 *   'settings_description' => a description of the provider that will be posted in the admin settings form
 *   'supported_features' => an array of rows describing the state of certain supported features by the provider.
 *    These will be rendered in a table, with the columns being 'Feature', 'Supported', 'Notes'.
 */
function emvideo_fact_info() {
  $features = array(
    array(t('Autoplay'), t('No'), ''),
    array(t('RSS Attachment'), t('Yes'), ''),
    array(t('Thumbnails'), t('Yes'), t('')),
    array(t('Full screen mode'), t('Yes'), t('You may customize the player to enable or disable full screen playback. Full screen mode is enabled by default.')),
  );
  return array(
    'provider' => 'fact',
    'name' => t('Fact'),
    'url' => EMVIDEO_FACT_MAIN_URL,
    'settings_description' => t('These settings specifically affect videos displayed from !fact. You can also read more about its !api.', array('!fact' => l(t('fact.tv'), EMVIDEO_FACT_MAIN_URL), '!api' => l(t("developer's API"), EMVIDEO_FACT_API_URL))),
    'supported_features' => $features,
  );
}

/**
 *  Implements emvideo_fact_validate().
 */
function emvideo_fact_validate($value, $error_field) {
  if ($value == 'FACT_ERROR_EMBED') {
    form_set_error($error_field, t('Please do not use the embed code from fact.tv. You must instead paste the URL from the video page.'));
  }
}

/**
 *  Implementation of emvideo_fact_settings().
 *  This should return a subform to be added to the emvideo_settings() admin
 *  settings page.
 *
 *  Note that a form field set will already be provided at $form['fact'],
 *  so if you want specific provider settings within that field set, you should
 *  add the elements to that form array element.
 */
function emvideo_fact_settings() {
  $form['fact']['player_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Embedded video player options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['fact']['player_options']['emvideo_fact_full_screen'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow fullscreen'),
    '#default_value' => variable_get('emvideo_fact_full_screen', 1),
    '#description' => t('Allow users to view video using the entire computer screen.'),
  );

  return $form;
}

/**
 *  Implementation of emvideo_fact_extract().
 *
 *  This is called to extract the video code from a pasted URL.
 *
 *  We'll be passed a URL from a video when an editor pastes
 *  that in the field's textfield. We'll need to either pass back an array of
 *  regex expressions to match, or do the matching ourselves and return the
 *  resulting video code.
 *
 *  @param $parse
 *    An optional string with the pasted URL.
 *  @return
 *    Either an array of regex expressions to be tested, or a string with the
 *    video code to be used. If the hook tests the code itself, it should
 *    return either the string of the video code (if matched), or an empty
 *    array. Otherwise, the calling function will handle testing the embed code
 *    against each regex string in the returned array.
 */
function emvideo_fact_extract($embed) {
  // Here we assume that a URL will be passed in the form of
  // http://www.fact.tv/videos/watch/739
  // We'll simply return an array of regular expressions for Embedded Media
  // Field to handle for us.
  return array('@fact\.tv/videos/watch/([^\?]+)@i');
}

/**
 *  Implementation of emvideo_fact_data().
 *
 *  Provides an array to be serialised and made available with $item elsewhere.
 *
 *  This data can be used to store any extraneous information available
 *  specifically to the fact provider.
 */
function emvideo_fact_data($field, $item, $error_field = '') {
  // Initialize the data array.
  $data = array();

  // Create some version control. Thus if we make changes to the data array
  // down the road, we can respect older content. If allowed by Embedded Media
  // Field, any older content will automatically update this array as needed.
  // In any case, you should account for the version if you increment it.
  $data['emvideo_fact_version'] = EMVIDEO_FACT_DATA_VERSION;

  $xml_url = "http://www.fact.tv/videos/watch/" . $item['value'] . ".xml";
  $loop_count = 0;
  $headers = array();
  $data = array();

  // For some reason, FACT like to redirect the request several times, so we need to cope with it.
  // We also need to accept their cookies to get access to the xml.
  do {
    $http_result = drupal_http_request($xml_url, $headers, $method = 'GET', $data = NULL, $retry = 3);
    if (isset($http_result->headers['Set-Cookie'])) {
      $cookie = $http_result->headers['Set-Cookie'];
      $headers = array('Cookie' => $cookie);
    }
    if (($http_result->code == '301') || ($http_result->code == '302')) {
      $xml_url = $http_result->redirect_url;
    }
    $loop_count++;
  }
  while (($loop_count < 5) && ($http_result->code != '200'));

  if ($http_result->code == '200') {
    @$xml_content = simplexml_load_string($http_result->data);
    if (isset($xml_content->channel->item[1]) && is_object($xml_content->channel->item[1])) {
      $media = $xml_content->channel->item[1]->children('http://search.yahoo.com/mrss/');

      // Extract the flash video URL.
      $attr = $media->content->attributes();
      $flv_url = $attr['url'];

      // Extract the thumbnail URL.
      $attr = $media->thumbnail->attributes();
      $thumbnail_url = $attr['url'];

      // Store the URL to the video's flash file.
      $data['flv_url'] = check_url(EMVIDEO_FACT_MAIN_URL . $flv_url);

      // Store the URL of the thumbnail image.
      $data['thumbnail'] = check_url(EMVIDEO_FACT_MAIN_URL . $thumbnail_url);
    }
    else {
      // FACT will return a 200 code even if the video id doesn't exist.  This covers that scenario.
      form_set_error($error_field, t('The XML page for the item at fact.tv could not be retrieved.'));
    }
  }
  else {
    // Error message to display if FACT returns a code other than 200.
    form_set_error($error_field, t('The XML page for the item at fact.tv could not be retrieved: @code: @error', array('@code' => $http_result->code, '@error' => $http_result->status_message)));
  }
  return $data;
}

/**
 *  Implementation of emvideo_fact_rss().
 *
 *  This attaches a file to an RSS feed.
 */
function emvideo_fact_rss($item, $teaser = NULL) {
  if ($item['value']) {
    $file['thumbnail']['filepath'] = $item['data']['thumbnail'];
    return $file;
  }
}

/**
 * Implementation of emvideo_fact_embedded_link().
 * returns a link to view the video at the provider's site.
 *  @param $video_code
 *    The string containing the video to watch.
 *  @return
 *    A string containing the URL to view the video at the original provider's site.
 */
function emvideo_fact_embedded_link($video_code) {
  return 'http://www.fact.tv/videos/watch/'. $video_code;
}

/**
 * The embedded flash displaying the fact video.
 */
function theme_emvideo_fact_flash($item, $width, $height, $autoplay) {
  $output = '';
  if ($item['embed']) {
    $autoplay = $autoplay ? 'true' : 'false';
    $flashvars = "file=" . $item['data']['flv_url'];
    $fullscreen = variable_get('emvideo_fact_full_screen', 1) ? 'true' : 'false';
    $output = "<embed width='$width' height='$height' bgcolor='#000' allowscriptaccess='always' allowfullscreen='$fullscreen' flashvars='$flashvars' src='http://fact.tv/flash/player.swf' />";
  }
  return $output;
}

/**
 * Implementation of emvideo_fact_thumbnail().
 * Returns the external url for a thumbnail of a specific video.
 *  @param $field
 *    The field of the requesting node.
 *  @param $item
 *    The actual content of the field from the requesting node.
 *  @return
 *    A URL pointing to the thumbnail.
 */
function emvideo_fact_thumbnail($field, $item, $formatter, $node, $width, $height) {
  if (isset($item['data']['thumbnail'])) {
    return $item['data']['thumbnail'];
  }
}

/**
 *  Implementation of emvideo_fact_video().
 *  This actually displays the full/normal-sized video we want, usually on the
 *  default page view.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_fact_video($embed, $width, $height, $field, $item, $node, $autoplay) {
  $output = theme('emvideo_fact_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 *  Implementation of emvideo_fact_video().
 *
 *  This actually displays the preview-sized video we want, commonly for the
 *  teaser.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_fact_preview($embed, $width, $height, $field, $item, $node, $autoplay) {
  $output = theme('emvideo_fact_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 * Implementation of hook_emfield_subtheme().
 * This returns any theme functions defined by this provider.
 */
function emvideo_fact_emfield_subtheme() {
  $themes = array(
    'emvideo_fact_flash'  => array(
      'arguments' => array('item' => NULL, 'width' => NULL, 'height' => NULL, 'autoplay' => NULL),
      'file' => 'includes/providers/fact.inc',
      'path' => drupal_get_path('module', 'fact'),
    )
  );
  return $themes;
}

/**
 *  Implement hook_emvideo_PROVIDER_content_generate().
 */
function emvideo_vimeo_content_generate() {
  return array(
    'http://fact.tv/videos/watch/739',
    'http://fact.tv/videos/watch/691',
    'http://fact.tv/videos/watch/767',
    'http://fact.tv/videos/watch/775',
  );
}